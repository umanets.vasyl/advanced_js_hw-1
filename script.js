class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get name() {
    return this.name;
  }
  set name(value) {
    this._name = value;
  }

  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }

  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return this._salary;
  }

  set salary(value) {
    super.salary = value;
    this._salary *= 3;
  }
}

let Programmer_one = new Programmer("Audrey Leon", 45, 4100, ["C#", "Python"]);
let Programmer_two = new Programmer("Gerard Solis", 24, 2300, [
  "C++",
  "JavaScript",
]);
let Programmer_three = new Programmer("Bryan Brooks", 27, 8000, [
  "C++",
  "Python",
  "JavaScript",
  "R",
]);

console.log(Programmer_one);
console.log(Programmer_two);
console.log(Programmer_three);
